//
//  ThanksViewController.swift
//  AEONSafetyProject
//
//  Created by Dong Nguyen on 7/6/19.
//  Copyright © 2019 Dong Nguyen. All rights reserved.
//

import UIKit

class ThanksViewController: UIViewController {
    
    //MARK: IBOUTLETS
    
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var lbDesc: UILabel!
    
    //MARK: OTHER VARIABLES
    var dismissHandler : (()->())?
    var countOfTrue = 0
    //MARK: VIEW LIFE CYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupUI()
        setupVar()
        callAPI()
    }
    
    //MARK: - SETUP VAR
    func setupVar() {
        
    }
    
    //MARK: - SETUP UI
    func setupUI() {
        let isSucces = countOfTrue >= TRUE_NEEDED
        if isSucces {
            imgBg.image = #imageLiteral(resourceName: "bg_gift")
            lbDesc.isHidden = false
            var str = "đã hoàn thành tất cả các câu hỏi!\nMời bạn nhận 01 phần quà."
            if countOfTrue < MAX_QUESTION {
                str = "đã hoàn thành \(countOfTrue)/\(MAX_QUESTION) câu hỏi!\nMời bạn nhận 01 phần quà."
            }
            let attr = NSMutableAttributedString.init(string: str.uppercased())
            attr.addStrokeColor(string: "01", color: .red, strokeColor: .white)
            lbDesc.attributedText = attr
            
        } else {
            imgBg.image = #imageLiteral(resourceName: "bg_thanks")
            lbDesc.isHidden = true
        }
    }
    
    //MARK: - CALL API
    func callAPI() {
        
        fillData()
    }
    
    //MARK: - FILL AND BIND DATA
    func fillData() {
        
    }
    
    //MARK: - BUTTON ACTIONS
    @IBAction func didTouchBtnBack(_ sender: Any) {
        self.dismiss(animated: false) {
            if self.dismissHandler != nil {
                self.dismissHandler!()
            }
        }
    }
    
}

//
//  Constant.swift
//  AEONSafetyProject
//
//  Created by Dong Nguyen on 7/6/19.
//  Copyright © 2019 Dong Nguyen. All rights reserved.
//

import Foundation
import UIKit

let STORYBOARD_MAIN = UIStoryboard.init(name: "Main", bundle: nil)
let RED_COLOR = UIColor.init(hex: "5d2314")
let BROWN_COLOR = UIColor.init(hex: "5d2314")
let BG_COLOR = UIColor.init(hex: "fff2ca")

let MAX_QUESTION = 6
let TRUE_NEEDED = 5

//MESS
let kMsgTrue = "Chúc mừng bạn đã trả lời đúng"
let kMsgFalse = "Câu trả lời đúng là\n%@"
let kMsgSelectCategory = "Vui lòng chọn chủ đề"

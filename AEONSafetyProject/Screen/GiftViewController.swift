//
//  GiftViewController.swift
//  AEONSafetyProject
//
//  Created by Dong Nguyen on 7/6/19.
//  Copyright © 2019 Dong Nguyen. All rights reserved.
//

import UIKit

class GiftViewController: UIViewController {
    
    //MARK: IBOUTLETS
    
    
    //MARK: OTHER VARIABLES
    var dismissHandler : (()->())?
    
    //MARK: VIEW LIFE CYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupUI()
        setupVar()
        callAPI()
    }
    
    //MARK: - SETUP VAR
    func setupVar() {
        
    }
    
    //MARK: - SETUP UI
    func setupUI() {
        
    }
    
    //MARK: - CALL API
    func callAPI() {
        
        fillData()
    }
    
    //MARK: - FILL AND BIND DATA
    func fillData() {
        
    }
    
    //MARK: - BUTTON ACTIONS
    
}

//
//  ViewController.swift
//  AEONSafetyProject
//
//  Created by Dong Nguyen on 7/5/19.
//  Copyright © 2019 Dong Nguyen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: IBOUTLETS
    
    
    //MARK: OTHER VARIABLES
    
    
    //MARK: VIEW LIFE CYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupUI()
        setupVar()
        callAPI()
    }
    
    //MARK: - SETUP VAR
    func setupVar() {
        
    }
    
    //MARK: - SETUP UI
    func setupUI() {
        
    }
    
    //MARK: - CALL API
    func callAPI() {
        
        fillData()
    }
    
    //MARK: - FILL AND BIND DATA
    func fillData() {
        
    }
    
    //MARK: - BUTTON ACTIONS
    
    @IBAction func didTouchBtnExport(_ sender: Any) {
        Utils.shared.createFileCSV()
        Utils.shared.shareFile(rootView: self)
    }
}


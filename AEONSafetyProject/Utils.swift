//
//  Utils.swift
//  AEONSafetyProject
//
//  Created by Dong Nguyen on 7/7/19.
//  Copyright © 2019 Dong Nguyen. All rights reserved.
//

import UIKit
let FILE_NAME = "analytics"
class Utils: NSObject {
    
    static var shared = Utils()
    
    func createFileCSV() {
        let fileName = "\(FILE_NAME).csv"
        let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
        var csvText = "Ho ten phu huynh,Ho ten be,Dia chi,So dien thoai\n"
        if let data = UserDefaults.standard.array(forKey: FILE_NAME) as? [[String: Any]] {
            for item in data {
                guard let model : InfoModel = try? unbox(dictionary: item)  else {
                    return
                }
                let newLine = "\(model.parentName ?? ""),\(model.name ?? ""),\(model.address ?? ""),\(model.phone ?? "")\n"
                csvText.append(newLine)
            }
        }
        
        
        do {
            try csvText.write(to: path!, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print("Failed to create file")
            print("\(error)")
        }
        print(path ?? "not found")
    }
    
    func shareFile(rootView : UIViewController) {
        let fileName = "\(FILE_NAME).csv"
        if let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName) {
            let vc = UIActivityViewController(activityItems: [path], applicationActivities: nil)
            
            rootView.present(vc, animated: true, completion: nil)
            if let popOver = vc.popoverPresentationController {
                popOver.sourceView = rootView.view
            }
        } else {
            rootView.showAlert(title: "Chưa có dữ liệu", message: nil)
        }
    }
    
    func saveInfo(info : InfoModel) {
        if var data = UserDefaults.standard.array(forKey: FILE_NAME) as? [[String: Any]] {
            data.append(info.toDictionary())
            UserDefaults.standard.set(data, forKey: FILE_NAME)
        } else {
            UserDefaults.standard.set([info.toDictionary()], forKey: FILE_NAME)
        }
    }
}

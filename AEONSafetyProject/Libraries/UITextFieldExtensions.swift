//
//  UITextFieldExtensions.swift
//  SwifterSwift
//
//  Created by Omar Albeik on 8/5/16.
//  Copyright © 2016 Omar Albeik. All rights reserved.
//

#if os(iOS) || os(tvOS)
import UIKit
// MARK: - Methods
public extension UITextField {
	
	/// SwifterSwift: Clear text.
    func clear() {
		text = ""
		attributedText = NSAttributedString(string: "")
	}
	
    @IBInspectable var PlaceHolderTextColor: UIColor {
        get { return .clear }
        set(value) {
            setPlaceHolderTextColor(value)
        }
    }
    
	/// SwifterSwift: Set placeholder text color.
	///
	/// - Parameter color: placeholder text color.
    func setPlaceHolderTextColor(_ color: UIColor) {
		guard let holder = placeholder, !holder.isEmpty else {
			return
		}
        self.attributedPlaceholder = NSAttributedString(string: holder, attributes: [NSAttributedString.Key.foregroundColor: color])
	}
  
   
}
	
#endif

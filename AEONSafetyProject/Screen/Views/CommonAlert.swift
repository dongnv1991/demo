//
//  CommonAlert.swift
//  KeyStoneNew
//
//  Created by Dong Nguyen on 2/27/17.
//  Copyright © 2017 TVT25. All rights reserved.
//

import UIKit

class CommonAlert: UIView, UITextFieldDelegate {
    
    //MARK: - VAR
    
    @IBOutlet weak var btnDismiss: UIButton!
    
    @IBOutlet weak var lbTitle: UILabel!
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var btnAction: UIButton!
    
    @IBOutlet weak var imgBackground: UIImageView!
    var dismissBlock : (() -> ())?
    var didTouchDescHandler : (() -> ())?
    //MARK: - SELF
    
    class func instanceFromNib() -> CommonAlert {
        return UINib(nibName: "CommonAlert", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CommonAlert
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func show(title : String, dismissHandler : (() -> ())?) {
        guard let topView = UIApplication.shared.keyWindow else {
            return
        }
        self.dismissBlock = nil
        self.dismissBlock = dismissHandler
        self.frame = topView.bounds
        self.alpha = 0
        self.lbTitle.text = title
        topView.addSubview(self)
        contentView.alpha = 0.0
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: { () -> Void in
            self.alpha = 1.0
        }, completion: nil)
        
        contentView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.2 , delay: 0.1, options: .curveEaseOut, animations: { () -> Void in
            self.contentView.alpha = 1.0
            self.contentView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: nil)
    }
    
    func showAlert(title : String, dismissHandler : (() -> ())?) {
        guard let topView = UIApplication.shared.keyWindow else {
            return
        }
        imgBackground.image = #imageLiteral(resourceName: "bg_sectioned")
        self.dismissBlock = nil
        self.dismissBlock = dismissHandler
        self.frame = topView.bounds
        self.alpha = 0
        self.lbTitle.text = title
        lbTitle.textColor = .white
        topView.addSubview(self)
        contentView.alpha = 0.0
        btnAction.setTitle("Đóng", for: .normal)
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: { () -> Void in
            self.alpha = 1.0
        }, completion: nil)
        
        contentView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.2 , delay: 0.1, options: .curveEaseOut, animations: { () -> Void in
            self.contentView.alpha = 1.0
            self.contentView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: nil)
    }
    
    func dismissPopup(completeHandler : (() -> ())?) {
        contentView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseIn, animations: { () -> Void in
            self.contentView.alpha = 0.0
            self.contentView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        }, completion: nil)
        
        UIView.animate(withDuration: 0.25, delay: 0.05, options: .curveEaseIn, animations: { () -> Void in
            self.alpha = 0.0
            
        }) { (completed) -> Void in
            self.removeSubviews()
            if completeHandler != nil {
                completeHandler!()
            }
        }
        
    }
    
    //MARK: - BUTTON ACTION
    
    @IBAction func didTouchBtnDismiss(_ sender: Any) {
        self.dismissPopup(completeHandler: self.dismissBlock)
    }
}

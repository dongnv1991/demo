//
//  EnterInfoViewController.swift
//  AEONSafetyProject
//
//  Created by Dong Nguyen on 7/6/19.
//  Copyright © 2019 Dong Nguyen. All rights reserved.
//

import UIKit
class EnterInfoViewController: UIViewController {
    
    //MARK: IBOUTLETS
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var tfParentName: UITextField!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfAddress: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    
    
    //MARK: OTHER VARIABLES
    
    
    //MARK: VIEW LIFE CYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupUI()
        setupVar()
        callAPI()
    }
    
    //MARK: - SETUP VAR
    func setupVar() {
        
    }
    
    //MARK: - SETUP UI
    func setupUI() {
        
    }
    
    //MARK: - CALL API
    func callAPI() {
        
        fillData()
    }
    
    //MARK: - FILL AND BIND DATA
    func fillData() {
        
    }
    
    //MARK: - BUTTON ACTIONS
    
    @IBAction func didTouchBtnCheck(_ sender: Any) {
        btnCheck.isSelected = !btnCheck.isSelected
    }
    
    @IBAction func didTouchBtnRegister(_ sender: Any) {
        let alert = CommonAlert.instanceFromNib()
        
        
        guard let parentName = tfParentName.text, parentName.trimmingCharacters(in: .whitespacesAndNewlines).count > 0  else {
            alert.showAlert(title: "Vui lòng nhập họ tên phụ huynh") {
                self.tfParentName.becomeFirstResponder()
            }
            return
        }
        guard let name = tfName.text, name.trimmingCharacters(in: .whitespacesAndNewlines).count > 0  else {
            alert.showAlert(title: "Vui lòng nhập họ tên bé") {
                self.tfName.becomeFirstResponder()
            }
            return
        }
        guard let address = tfAddress.text, address.trimmingCharacters(in: .whitespacesAndNewlines).count > 0  else {
            alert.showAlert(title: "Vui lòng nhập địa chỉ") {
                self.tfAddress.becomeFirstResponder()
            }
            return
        }
        guard let phone = tfPhone.text, phone.trimmingCharacters(in: .whitespacesAndNewlines).count > 0  else {
            alert.showAlert(title: "Vui lòng nhập số điện thoại") {
                self.tfPhone.becomeFirstResponder()
            }
            return
        }
        if !btnCheck.isSelected {
            alert.showAlert(title: "Vui lòng đồng ý với thể lệ của chương trình", dismissHandler: nil)
            return
        }
        var info = InfoModel.init()
        info.name = name
        info.parentName = parentName
        info.address = address
        info.phone = phone
        Utils.shared.saveInfo(info: info)
        let vc = STORYBOARD_MAIN.instantiateViewController(withIdentifier: "CategoryViewController")
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension EnterInfoViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        guard let text = textField.text else {
//            return true
//        }
//        let new = (text as NSString).replacingCharacters(in: range, with: string)
//        if textField == tfName {
//            return new.contains(kName)
//        } else if textField == tfParentName {
//            return new.contains(kParentName)
//        } else if textField == tfPhone {
//            return new.contains(kPhone)
//        } else if textField == tfAddress {
//            return new.contains(kAddress)
//        }
        return true
    }
    
    
}

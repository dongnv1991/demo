//
//  QuestionModel.swift
//  AEONSafetyProject
//
//  Created by Dong Nguyen on 7/7/19.
//  Copyright © 2019 Dong Nguyen. All rights reserved.
import Foundation

struct QuestionData: Unboxable {
    
    public var data : [Question]?
    
    init(unboxer: Unboxer) throws {
        data = try? unboxer.unbox(key: "data")
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if data != nil {
            var dictionaryElements = [[String:Any]]()
            for dataElement in data! {
                dictionaryElements.append(dataElement.toDictionary())
            }
            dictionary["data"] = dictionaryElements
        }
        return dictionary
    }
}
struct Question: Unboxable {
    
    public var answer : [String]?
    public var choose : String?
    public var question : String?
    
    init() {
        
    }
    
    init(unboxer: Unboxer) throws {
        answer = try? unboxer.unbox(key: "answer")
        choose = try? unboxer.unbox(key: "choose")
        question = try? unboxer.unbox(key: "question")
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if answer != nil{
            dictionary["answer"] = answer
        }
        if choose != nil{
            dictionary["choose"] = choose
        }
        if question != nil{
            dictionary["question"] = question
        }
        return dictionary
    }
}

struct InfoModel: Unboxable {
    
    public var parentName : String?
    public var name : String?
    public var address : String?
    public var phone : String?
    
    init() {
        
    }
    
    init(unboxer: Unboxer) throws {
        parentName = try? unboxer.unbox(key: "parentName")
        name = try? unboxer.unbox(key: "name")
        address = try? unboxer.unbox(key: "address")
        phone = try? unboxer.unbox(key: "phone")
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if parentName != nil{
            dictionary["parentName"] = parentName
        }
        if name != nil{
            dictionary["name"] = name
        }
        if address != nil{
            dictionary["address"] = address
        }
        if phone != nil{
            dictionary["phone"] = phone
        }
        return dictionary
    }
}

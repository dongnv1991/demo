//
//  CategoryViewController.swift
//  AEONSafetyProject
//
//  Created by Dong Nguyen on 7/6/19.
//  Copyright © 2019 Dong Nguyen. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController {
    
    //MARK: IBOUTLETS
    @IBOutlet weak var tableView: UITableView!
    var dataSource = ["Những nguy cơ trẻ em có thể bị xâm hại", "Phòng ngừa xâm hại trẻ em nơi công cộng", "Nguyên tắc tự bảo vệ bản thân cho trẻ", "Bảo vệ trẻ em trước nguy cơ bị xâm hại"]
    //MARK: OTHER VARIABLES
    
    
    //MARK: VIEW LIFE CYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupUI()
        setupVar()
        callAPI()
    }
    
    //MARK: - SETUP VAR
    func setupVar() {
        
    }
    
    //MARK: - SETUP UI
    func setupUI() {
        tableView.tableFooterView = UIView()
    }
    
    //MARK: - CALL API
    func callAPI() {
        
        fillData()
    }
    
    //MARK: - FILL AND BIND DATA
    func fillData() {
        
    }
    
    func readFile(_ index : Int) {
        self.readJson(fileName: "\(index)") { (data) in
            guard let result = data as? [String : Any] else {return}
            guard let model : QuestionData = try? unbox(dictionary: result)  else {
                return
            }
            let vc = STORYBOARD_MAIN.instantiateViewController(withIdentifier: "QuestionViewController") as! QuestionViewController
            vc.questions = (model.data ?? []).shuffled()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func readJson(fileName : String, completeHanler : ((_ data : Any?)->())) {
        do {
            if let file = Bundle.main.url(forResource: fileName, withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    print(object)
                    completeHanler(object)
                } else if let object = json as? [Any] {
                    // json is an array
                    print(object)
                    completeHanler(object)
                } else {
                    print("JSON is invalid")
                    completeHanler(nil)
                }
            } else {
                print("no file")
                completeHanler(nil)
            }
        } catch {
            print(error.localizedDescription)
            completeHanler(nil)
        }
    }
    
    //MARK: - BUTTON ACTIONS
    
    @IBAction func didTouchBtnNext(_ sender: Any) {
        
        if let index = tableView.indexPathForSelectedRow?.row {
            readFile(index + 1)
        } else {
            let alert = CommonAlert.instanceFromNib()
            alert.showAlert(title: kMsgSelectCategory, dismissHandler: nil)
        }
    }
}

extension CategoryViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableViewCell", for: indexPath) as! CategoryTableViewCell
        let row = indexPath.row
        cell.lbTitle.text = dataSource[row]
        cell.lbNum.text = "\(row + 1)"
        return cell
    }
    
    
}

class CategoryTableViewCell : UITableViewCell {
    
    @IBOutlet weak var lbNum: UILabel!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var imgBg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            lbTitle.textColor = BG_COLOR
            imgBg.image = #imageLiteral(resourceName: "bg_sectioned")
        } else {
            lbTitle.textColor = BROWN_COLOR
            imgBg.image = #imageLiteral(resourceName: "bg_section")
        }
    }
}

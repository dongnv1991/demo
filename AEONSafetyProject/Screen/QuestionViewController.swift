//
//  QuestionViewController.swift
//  AEONSafetyProject
//
//  Created by Dong Nguyen on 7/6/19.
//  Copyright © 2019 Dong Nguyen. All rights reserved.
//

import UIKit

class QuestionViewController: UIViewController {
    
    //MARK: IBOUTLETS
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lbQuestion: UILabel!
    @IBOutlet weak var imgScroll: UIImageView!
    
    //MARK: OTHER VARIABLES
    var selectquestion = Question() {
        didSet {
            lbQuestion.text = selectquestion.question ?? ""
            self.tableView.scroll(to: .top, animated: false)
            self.tableView.reloadData {
                self.imgScroll.isHidden = self.tableView.bounds.height > self.tableView.contentSize.height
            }
        }
    }
    var countOfTrue = 0
    var questionIndex = 0 {
        didSet {
            if questionIndex == MAX_QUESTION {
                checkResult()
            } else {
                if questionIndex < questions.count {
                    selectquestion = questions[questionIndex]
                }
            }
        }
    }
    var questions = [Question]()
    //MARK: VIEW LIFE CYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setupUI()
        setupVar()
        callAPI()
    }
    
    //MARK: - SETUP VAR
    func setupVar() {
        questionIndex = 0
    }
    
    //MARK: - SETUP UI
    func setupUI() {
        tableView.tableFooterView = UIView()
        imgScroll.image = UIImage.gifImageWithName("scroll")
    }
    
    func checkResult() {
        let vc = STORYBOARD_MAIN.instantiateViewController(withIdentifier: "ThanksViewController") as! ThanksViewController
        vc.countOfTrue = countOfTrue
        vc.dismissHandler = {
            self.navigationController?.popToRootViewController(animated: false)
        }
        present(vc, animated: true, completion: nil)
    }
    //MARK: - CALL API
    func callAPI() {
        
        fillData()
    }
    
    //MARK: - FILL AND BIND DATA
    func fillData() {
        
    }
    
    //MARK: - BUTTON ACTIONS
    
    @IBAction func didTouchBtnNext(_ sender: Any) {
        if let answer = selectquestion.answer, let index = tableView.indexPathForSelectedRow?.row {
            if selectquestion.choose == answer[index] {
                let alert = CommonAlert.instanceFromNib()
                alert.show(title: kMsgTrue) {
                    self.countOfTrue += 1
                    self.questionIndex += 1
                }
            } else {
                let alert = CommonAlert.instanceFromNib()
                alert.show(title: String.init(format: kMsgFalse, selectquestion.choose ?? "")) {
                    self.questionIndex += 1
                }
            }
        }
    }
}

extension QuestionViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectquestion.answer?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionTableViewCell", for: indexPath) as! QuestionTableViewCell
        let index = indexPath.row
        if let answer = selectquestion.answer {
            cell.lbTitle.text = answer[index]
        }
        return cell
    }
    
    
}

class QuestionTableViewCell : UITableViewCell {
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            imgCheck.image = #imageLiteral(resourceName: "ic_checked")
        } else {
            imgCheck.image = #imageLiteral(resourceName: "ic_check")
        }
    }
}



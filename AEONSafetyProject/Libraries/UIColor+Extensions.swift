//
//  UIColor+Extensions.swift
//  Kiple
//
//  Created by Dong Nguyen on 7/20/17.
//  Copyright © 2017 com.futurify.vn. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat = 1.0) {
        self.init(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: a)
    }
    
    public class func imageWithColor(color : UIColor) -> UIImage {
        let rect = CGRect.init(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
    
    convenience init(hex: String, alpha : CGFloat) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: alpha
        )
    }
}

extension NSMutableAttributedString {
    
    public func color(string : String, color : UIColor)  -> Self {
        let attributedString = NSAttributedString.init(string: string, attributes: [NSAttributedString.Key.foregroundColor : color])
        self.append(attributedString)
        return self
    }
    
    public func addStrokeColor(string : String, color : UIColor, strokeColor : UIColor){
        let currentString = NSString.init(string: self.string)
        self.addAttributes([.strokeWidth: -1.0, .strokeColor: strokeColor, .foregroundColor: color], range: currentString.range(of: string))
    }
    
    
    public func backgroundColorColor(string : String, color : UIColor)  -> Self {
        let attributedString = NSAttributedString.init(string: string, attributes: [NSAttributedString.Key.backgroundColor : color])
        self.append(attributedString)
        return self
    }
    
    func addFont(string : String, font : UIFont){
        
        let currentString = NSString.init(string: self.string)
        self.addAttribute(NSAttributedString.Key.font, value: font, range: currentString.range(of: string))
    }
    
    func addColor(string : String, color : UIColor){
        let currentString = NSString.init(string: self.string)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: currentString.range(of: string))
    }
    
    func addBackgroundColor(string : String, color : UIColor){
        let currentString = NSString.init(string: self.string)
        self.addAttribute(NSAttributedString.Key.backgroundColor, value: color, range: currentString.range(of: string))
    }
    
    func addUnderline(string : String){
        let currentString = NSString.init(string: self.string)
        self.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: currentString.range(of: string))
    }
    
    func addParagraphStyle(string : String, style : NSMutableParagraphStyle){
        let currentString = NSString.init(string: self.string)
        self.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: currentString.range(of: string))
    }
}
